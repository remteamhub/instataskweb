<?php

namespace App\Console;

use App\Console\Commands\cancelJob;
use App\Console\Commands\dispatchJob;
use App\Console\Commands\providerReInitStartOfDay;
use App\Console\Commands\timeExeedNotification;
use App\Console\Commands\timeOutJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\dispatchJob',
        'App\Console\Commands\cancelJob',
        'App\Console\Commands\timeOutJob',
        'App\Console\Commands\timeExeedNotification',
        'App\Console\Commands\providerReInitStartOfDay',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//         $schedule->command(dispatchJob::class)->everyMinute();
         $schedule->command(timeOutJob::class)->everyFiveMinutes();
         $schedule->command(cancelJob::class)->everyTenMinutes();
         $schedule->command(providerReInitStartOfDay::class)->everyMinute();
         $schedule->command(timeExeedNotification::class)->everyTenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
