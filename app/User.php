<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /*
     * constants for status of users
     */

    public const active = 0;
    public const inactive = 1;
    public const banned = 2;
    public const pending = 3;

    /*
     * constants for type of users
     */

    public const customer = 0;
    public const provider = 1;
    public const admin = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'username',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'phone',
        'avatar',
        'address',
        'fcm_token',
        'socket_token',
        'social_id',
        'lat',
        'lng',
        'status',
        'first_name',
        'last_name',
        'type',
        'time_zone',
        'stripe_unique_id',
        'rating'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
