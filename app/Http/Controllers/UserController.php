<?php

namespace App\Http\Controllers;

use App\Mail\defaultNotify;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    //

    public function index()
    {
        $users = User::where('type', '=', User::customer)->orderBy('id', 'desc')->paginate(8);
        return view('users.index', compact('users'));
    }

    public function form(Request $request)
    {
        if (isset($request->id) && $request->id != 0) {
            $results = $this->edit($request->id);
            if ($results instanceof User) {
                $page_title = "Edit";
//                dd($results);
                return view('users.form', compact('page_title', 'results'));
            }
            return $results;
        } else {
            $page_title = "Add";
            return view('users.form', compact('page_title'));
        }
    }

    public function edit($id)
    {
        $user = User::find($id);
        if (!$id || !$user) {
            return redirect()->back();
        } else {
            return $user;
        }
    }

    public function create($request)
    {

        $validation_rules = [
            'username' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email',
            'phone' => 'required',
            'address' => 'required',
        ];
        $request->validate($validation_rules);

        $request->password = generateRandomString();
        $hash_pass = Hash::make($request->password);
        $response = [];
        $data = [
            'username' => $request->username,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'status' => $request->status,
            'password' => $hash_pass
        ];
//            if ($request->password) {
//                $user->password = Hash::make($request->password);
//            }

        if ($request->hasFile('avatar_file')) {
            $data['avatar'] = $request->file('avatar_file')->store('avatar_file');
        }

        $usr = User::create($data);
        $body = "<p>Thanks for joining.</p>
                    we are pleased to inform you of the your account is approved and created successfully. Kindly use this
                    password: " . $request->password . " to login to the application and change this password by going to update profile from application menu.</p>
                    <br><br><p>
                    Regards Instatask</p>";
        if ($usr) {
            Mail::to($request->email)->send(new defaultNotify("Welcome to instatask", $body));
            $response = ['success_msg' => trans('alert.record_updated')];
        } else {
            $response = ['error_msg' => trans('alert.record_unable_to_save')];
        }
        return redirect(route('all-users'))->withErrors($response);
    }

    public function update($request)
    {

        $validation_rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ];
        $request->validate($validation_rules);

        $response = [];
        $data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'address' => $request->address,
            'status' => $request->status
        ];
        if ($request->hasFile('avatar_file')) {
            $data['avatar'] = $request->file('avatar_file')->store('avatar_file');
        }
        $user = User::find($request->id);
        if ($user->update($data)) {
            if($request->status == User::banned) {
                $body = "<p>Your profile is banned by company!.</p>
                    we are hereby to inform you that the your account is banned by company. Kindly inform this
                    to our help center if this is mistake by company.</p>
                    <br><br><p>
                    Regards Instatask</p>";
                Mail::to($user->email)->send(new defaultNotify("Profile Banned!", $body));
            }else {
                $body = "<p>Your profile is recently updated!.</p>
                    we are hereby to inform you of the your account is recently updated. Kindly inform this
                    to our help center if this is not done by you.</p>
                    <br><br><p>
                    Regards Instatask</p>";
                Mail::to($user->email)->send(new defaultNotify("Profile update", $body));
            }
            $response = ['success_msg' => trans('alert.record_updated')];
        } else {
            $response = ['error_msg' => trans('alert.record_unable_to_save')];
        }

        return redirect(route('edit-users', ['id' => $user->id]))->withErrors($response);
    }

    public function profile(Request $request)
    {
        if ($request->isMethod('post')) {
            if ($request->id == 0) {
                return $this->create($request);
            } else {
                return $this->update($request);
            }
        }

        $response = ['error_msg' => trans('alert.invalid_request')];

        return redirect(route('all-users'))->withErrors($response);
    }

    public function status(Request $request)
    {
        $response = [];
        $user = User::find($request->id);
        $data['status'] = $request->status;
        if ($user->update($data)) {
            if($request->status == User::banned) {
                $body = "<p>Your profile is banned by company!.</p>
                    we are hereby to inform you that the your account is banned by company. Kindly inform this
                    to our help center if this is mistake by company.</p>
                    <br><br><p>
                    Regards Instatask</p>";
                Mail::to($user->email)->send(new defaultNotify("Profile Banned!", $body));
            }
            $response = ['success_msg' => trans('alert.record_updated')];
        } else {
            $response = ['error_msg' => trans('alert.record_unable_to_save')];
        }
        return redirect(route('all-users'))->withErrors($response);
    }

    public function search(Request $request)
    {
        $users = User::where('first_name','LIKE','%'.$request->user.'%')->orWhere('last_name','LIKE','%'.$request->user.'%')->get();
        return view('users.partials.result',compact('users'));
    }
}
