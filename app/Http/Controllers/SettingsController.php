<?php

namespace App\Http\Controllers;

use App\Service;
use App\System_prefrence;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    //
    public function index()
    {
        $services = Service::all();
        $page_title = "Prefrences Price";
        return view('services.index', compact('page_title', 'services'));
    }

    public function updatePrice(Request $request)
    {
//        dd($request->all());
        $single_lane = [
                        'price' => $request->single_lane_price,
                        'commision' => $request->single_lane_commision
                        ];
        $service = Service::where('title','=','1 Car Lane')->update($single_lane);

        $second_lane = [
            'price' => $request->second_lane_price,
            'commision' => $request->second_lane_commision
        ];
        $service = Service::where('title','=','2 Car Lanes')->update($second_lane);

        $third_lane = [
            'price' => $request->third_lane_price,
            'commision' => $request->third_lane_commision
        ];
        $service = Service::where('title','=','3 Car Lanes')->update($third_lane);

        $response = ['success_msg' => trans('alert.record_updated')];
        return redirect(route('prefrences-price'))->withErrors($response);
    }

    public function indexLimitations(){
        $system_prefrence = System_prefrence::first();
        $page_title = "Prefrences Limitations";
        return view('limitations.index', compact('page_title', 'system_prefrence'));
    }

    public function updateLimitations(Request $request)
    {
        $data = [
            'max_job_limit' => $request->max_job_limit,
            'max_job_cancellation_time' => $request->max_job_cancellation_time,
            'max_job_acception_time' => $request->max_job_acception_time,
        ];
        $system_prefrence = System_prefrence::where('id','=',$request->id)->update($data);
        if($system_prefrence){
            $response = ['success_msg' => trans('alert.record_updated')];
        }else{
            $response = ['error_msg' => trans('alert.record_unable_to_save')];
        }
        return redirect(route('prefrences-limitations'))->withErrors($response);
    }
}
