<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\Provider;
use App\User;
use App\Service;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $driverStats = array();
        $jobStats = array();
        $userStats = array();
        $otherStats = array();

        $driverStats["totalDrivers"] = Provider::whereHas('userDetails')->count();
        $driverStats["onlineDrivers"] = Provider::whereHas('userDetails')->where([["job_status","!=",Provider::signedout]])->count();
        $driverStats["offlineDrivers"] = Provider::whereHas('userDetails')->where([["job_status","=",Provider::signedout]])->count();
        $driverStats["bannedDrivers"] = User::where([["type","=",User::provider],["status","=",User::banned]])->count();

        $jobStats["totalJobs"] = Job::count();
        $jobStats["cancelledJobs"] = Job::where("job_status",Job::cancel)->count();
        $jobStats["acceptedJobs"] = Job::where("job_status",Job::accept)->count();
        $jobStats["completedJobs"] = Job::where("job_status",Job::complete)->count();

        $userStats["totalUsers"] = User::where([["type","=",User::customer]])->count();
        $userStats["activeUsers"] = User::where([["type","=",User::customer],["status","=",User::active]])->count();
        $userStats["bannedUsers"] = User::where([["type","=",User::customer],["status","=",User::banned]])->count();

        $otherStats["totalservices"] = Service::count();
        $otherStats["totalDriverEarning"] = Provider::sum("total_earning");
        $otherStats["avgJobs"] = (Job::whereYear('created_at', '=', Carbon::now()->year)
                                    ->whereMonth('created_at', '=', Carbon::now()->month)
                                    ->count())/date("t");
        return view('home',compact("jobStats","driverStats","userStats","otherStats"));
    }
    public function delete(Request $request){
        $appPrefix = 'APP';
        $modelName = "$appPrefix \ $request->model";
        $modelName = str_replace(' ', '', $modelName);
        $modelValue=$modelName::find($request->id);
        if ($modelValue->delete()) {
            $response = ['success_msg' => trans('alert.record_delete_successfully')];
        } else {
            $response = ['error_msg' => trans('alert.record_unable_to_delete')];
        }
        return redirect(route($request->route))->withErrors($response);
    }
}
