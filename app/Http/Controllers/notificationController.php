<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Repository\NotificationRepository;
use Illuminate\Http\Request;

class notificationController extends Controller
{
    //
    public function viewNotifications(Request $request){
        NotificationRepository::markViewd($request->notification_id);
        return redirect(redirect(route('edit-jobs',['id' => $request->job_id])));
    }
}
