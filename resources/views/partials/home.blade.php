<div class="animated fadeIn">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">Jobs Statistics</h4>
                    <div class="small text-muted"></div>
                </div>
                <!-- /.col-->
                <div class="col-sm-7 d-none d-md-block">
                </div>
                <!-- /.col-->
            </div>
            <br>
            <!-- /.row-->
            <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-primary">
                    <div class="card-body pb-0">
                        <div class="text-value">
                        @if(isset($jobStats["totalJobs"]))
                        {{ $jobStats["totalJobs"] }}
                        @else
                        0
                        @endif
                        </div>
                        <div>Total Jobs</div>
                    </div><br><br>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-danger">
                    <div class="card-body pb-0">
                        <div class="text-value">
                        @if(isset($jobStats["cancelledJobs"]))
                        {{ $jobStats["cancelledJobs"] }}
                        @else
                        0
                        @endif
                        </div>
                        <div>Cancelled Jobs</div>
                    </div><br><br>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-warning">
                    <div class="card-body pb-0">
                        <div class="text-value">
                        @if(isset($jobStats["acceptedJobs"]))
                        {{ $jobStats["acceptedJobs"] }}
                        @else
                        0
                        @endif
                        </div>
                        <div>Accepted Jobs</div>
                    </div><br><br>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-success">
                    <div class="card-body pb-0">
                        <div class="text-value">
                        @if(isset($jobStats["completedJobs"]))
                        {{ $jobStats["completedJobs"] }}
                        @else
                        0
                        @endif
                        </div>
                        <div>Completed Jobs</div>
                    </div><br><br>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->
        </div>
    </div>
    <!-- /.card-->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">Drivers Statistics</h4>
                    <div class="small text-muted"></div>
                </div>
                <!-- /.col-->
                <div class="col-sm-7 d-none d-md-block">
                </div>
                <!-- /.col-->
            </div>
            <br>
            <!-- /.row-->
            <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-primary">
                    <div class="card-body pb-0">
                        <div class="text-value">
                        @if(isset($driverStats["totalDrivers"]))
                        {{ $driverStats["totalDrivers"] }}
                        @else
                        0
                        @endif
                        </div>
                        <div>Total Drivers</div>
                    </div><br><br>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-success">
                    <div class="card-body pb-0">
                        <div class="text-value">
                        @if(isset($driverStats["onlineDrivers"]))
                        {{ $driverStats["onlineDrivers"] }}
                        @else
                        0
                        @endif
                        </div>
                        <div>Online Drivers</div>
                    </div><br><br>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-warning">
                    <div class="card-body pb-0">
                        <div class="text-value">
                        @if(isset($driverStats["offlineDrivers"]))
                        {{ $driverStats["offlineDrivers"] }}
                        @else
                        0
                        @endif
                        </div>
                        <div>Offline Drivers</div>
                    </div><br><br>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-danger">
                    <div class="card-body pb-0">
                        <div class="text-value">
                        @if(isset($driverStats["bannedDrivers"]))
                        {{ $driverStats["bannedDrivers"] }}
                        @else
                        0
                        @endif
                        </div>
                        <div>Banned Drivers</div>
                    </div><br><br>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->
        </div>
    </div>
    <!-- /.card-->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">Customers Statistics</h4>
                    <div class="small text-muted"></div>
                </div>
                <!-- /.col-->
                <div class="col-sm-7 d-none d-md-block">
                </div>
                <!-- /.col-->
            </div>
            <br>
            <!-- /.row-->
            <div class="row">
            <div class="col-sm-6 col-lg-4">
                <div class="callout callout-info callout-bordered"><br>
                    <small class="text-muted">Total Customers</small>
                    <br>
                    <strong class="h4">
                    @if(isset($userStats["totalUsers"]))
                        {{ $userStats["totalUsers"] }}
                    @else
                    0
                    @endif
                    </strong><br><br>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="callout callout-success callout-bordered"><br>
                    <small class="text-muted">Active Customers</small>
                    <br>
                    <strong class="h4">
                    @if(isset($userStats["activeUsers"]))
                        {{ $userStats["activeUsers"] }}
                    @else
                    0
                    @endif
                    </strong><br><br>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="callout callout-danger callout-bordered"><br>
                    <small class="text-muted">Banned Customers</small>
                    <br>
                    <strong class="h4">
                    @if(isset($userStats["bannedUsers"]))
                        {{ $userStats["bannedUsers"] }}
                    @else
                        0
                    @endif
                    </strong><br><br>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->
        </div>
    </div>
    <!-- /.card-->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <h4 class="card-title mb-0">Other Statistics</h4>
                            <div class="small text-muted"></div>
                        </div>
                        <!-- /.col-->
                        <div class="col-sm-7 d-none d-md-block">
                        </div>
                        <!-- /.col-->
                    </div><br>
                    <table class="table table-responsive-sm table-hover table-outline mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th>Name</th>
                            <th>Value</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                No of tickets
                            </td>
                            <td>
                                <div>
                                    0
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                No of services
                            </td>
                            <td>
                                <div>
                                @if(isset($otherStats["totalservices"]))
                                    {{ $otherStats["totalservices"] }}
                                @else
                                    0
                                @endif
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Total Earned Money
                            </td>
                            <td>
                                <div>
                                    0
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Total Earning (Driver)
                            </td>
                            <td>
                                <div>
                                @if(isset($otherStats["totalDriverEarning"]))
                                    {{ $otherStats["totalDriverEarning"] }}
                                @else
                                    0
                                @endif
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Company Commission
                            </td>
                            <td>
                                <div>
                                    0
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Average Jobs / Day
                            </td>
                            <td>
                                <div>
                                @if(isset($otherStats["avgJobs"]))
                                    {{ $otherStats["avgJobs"] }}
                                @else
                                    0
                                @endif
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
</div>