<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin') }}">
                    <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>

            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-people"></i> Service Provider</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('all-providers') }}" target="_top">
                            <i class="nav-icon icon-list"></i> Provider List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('create-providers') }}" target="_top">
                            <i class="nav-icon icon-user-follow"></i> Provider Form</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-people"></i> Users</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('all-users') }}" target="_top">
                            <i class="nav-icon icon-list"></i> Users List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('create-users') }}" target="_top">
                            <i class="nav-icon icon-user-follow"></i> User Form</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon fa fa-briefcase"></i> Jobs</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('all-jobs') }}" target="_top">
                            <i class="nav-icon icon-list"></i> Jobs List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('create-jobs') }}" target="_top">
                            <i class="nav-icon fa fa-plus-square"></i> Job Form</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-settings"></i> System Preference</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('prefrences-price') }}" target="_top">
                            <i class="nav-icon fa fa-money"></i> Price</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('prefrences-limitations') }}" target="_top">
                            <i class="nav-icon fa fa-flag"></i> Limitations</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-settings"></i> Discount </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('create-discounts') }}" target="_top">
                            <i class="nav-icon fa fa-money"></i> Coupon </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('all-discounts') }}" target="_top">
                            <i class="nav-icon fa fa-money"></i>All Coupons </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-settings"></i> Help </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('all-guides') }}" target="_top">
                            <i class="nav-icon fa fa-money"></i>All Tickets </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>