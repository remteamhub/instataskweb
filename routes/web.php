<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

Auth::routes();

Route::group(['prefix' => 'admin'], function () {

    Route::get('/', function () {
        if (Auth::check()) {
            return Redirect::to('admin/home');
        }
        return view('auth.login');
    });

    Route::middleware(['auth'])->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/dispatcher', function () {
            return view('dispatcher.index');
        });
        Route::get('/delete/{model}/{route}/{id}', 'HomeController@delete')->name('delete-model');
        Route::group(['prefix' => 'prefrences'], function () {
            Route::get('/price', 'SettingsController@index')->name('prefrences-price');
            Route::post('/update-price', 'SettingsController@updatePrice')->name('update-price');
        });
        Route::group(['prefix' => 'settings'], function () {
            Route::get('/limitations', 'SettingsController@indexLimitations')->name('prefrences-limitations');
            Route::post('/update-limitations', 'SettingsController@updateLimitations')->name('update-limitations');
        });
        Route::group(['prefix' => 'users'], function () {
            Route::get('/all', 'UserController@index')->name('all-users');
            Route::get('/create', 'UserController@form')->name('create-users');
            Route::get('/edit/{id}', 'UserController@form')->name('edit-users');
            Route::post('/profile', 'UserController@profile')->name('profile-users');
            Route::get('/status/{status}/{id}', 'UserController@status')->name('status-users');
            Route::post('/search', 'UserController@search')->name('search-user');
        });
        Route::group(['prefix' => 'providers'], function () {
            Route::get('/all', 'ProviderController@index')->name('all-providers');
            Route::get('/create', 'ProviderController@form')->name('create-providers');
            Route::get('/edit/{id}', 'ProviderController@form')->name('edit-providers');
            Route::post('/profile', 'ProviderController@profile')->name('profile-providers');
            Route::get('/status/{status}/{id}', 'ProviderController@status')->name('status-providers');
            Route::get('/approval/{status}/{id}', 'ProviderController@approval')->name('approval-providers');
            Route::get('/job/{status}/{id}', 'ProviderController@job')->name('job-providers');
        });
        Route::group(['prefix' => 'jobs'], function () {
            Route::get('/all', 'JobsController@index')->name('all-jobs');
            Route::get('/create', 'JobsController@form')->name('create-jobs');
            Route::get('/edit/{id}', 'JobsController@form')->name('edit-jobs');
            Route::get('/status/{status}/{id}', 'JobsController@status')->name('status-jobs');
            Route::get('/approval/{status}/{id}', 'JobsController@approval')->name('approval-jobs');
            Route::get('/job/{status}/{id}', 'JobsController@job')->name('job-jobs');
            Route::get('/cancel/{model}/{route}/{id}', 'JobsController@cancelJob')->name('cancel-job');
            Route::post('/search', 'JobsController@search')->name('search-job');
        });
        Route::group(['prefix' => 'discounts'], function () {
            Route::get('/create', 'DiscountController@create')->name('create-discounts');
            Route::get('/all', 'DiscountController@all')->name('all-discounts');
            Route::post('/save', 'DiscountController@save')->name('save-discounts');
            Route::get('/edit/{id}', 'DiscountController@edit')->name('edit-discounts');
            Route::post('/update', 'DiscountController@update')->name('update-discounts');
            Route::get('/delete/{id}', 'DiscountController@delete')->name('delete-discounts');
        });
        Route::group(['prefix' => 'general'], function () {
            Route::get('/view-notifications/{job_id}/{notification_id}', 'notificationController@viewNotifications')->name('view-notifications');
        });
        Route::group(['prefix' => 'reports'], function () {
            Route::get('/users', 'ReportsController@usersReport')->name('users-report');
            Route::get('/users/csv', 'ReportsController@usersReportCsv')->name('users-report-csv');
            Route::get('/jobs/csv', 'ReportsController@jobsReportCsv')->name('jobs-report-csv');
            Route::get('/jobs', 'ReportsController@jobsReport')->name('jobs-report');
            Route::get('/jobs/complete', 'ReportsController@jobsCompleteReport')->name('jobs-complete-report');
        });
        Route::group(['prefix' => 'guides'], function () {
            Route::get('/create', 'GuideController@create')->name('create-guides');
            Route::get('/all', 'GuideController@all')->name('all-guides');
            Route::post('/save', 'GuideController@save')->name('save-guides');
            Route::get('/show/{id}', 'GuideController@show')->name('show-guides');
            Route::get('/delete/{id}', 'GuideController@delete')->name('delete-guides');
        });
    });
});
